import React from 'react';
import logo from './logo.svg';
import './App.css';
import StockItem from './StockItem';


function App() {
    return (
        <div className="App">
            <StockItem />
        </div>
    );
}

export default App;
