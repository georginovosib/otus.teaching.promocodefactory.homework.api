import { HttpTransportType, HubConnection, HubConnectionBuilder, LogLevel } from "@microsoft/signalr";
import { Button, Input, notification } from "antd";
import React, { useEffect, useState } from "react";

const StockItem = () => {
    const [connection, setConnection] = useState<null | HubConnection>(null);
    const [firstName, setfirstName] = useState("");
    const [lastName, setlastName] = useState("");

    useEffect(() => {

        console.log("start");

        const connect = new HubConnectionBuilder()
            .configureLogging(LogLevel.Debug)
            .withUrl("https://localhost:5001/hubs/greet", {
                skipNegotiation: true,
                transport: HttpTransportType.WebSockets
            })
            .withAutomaticReconnect()
            .build();

        setConnection(connect);
    }, []);

    useEffect(() => {
        if (connection) {
            connection
                .start()
                .then(() => {
                    console.log("connect to stock info");
                    connection.on("Receive", handleReceiveMessage);
                })
                .catch((error) => console.log(error));
        }
    }, [connection]);

    const handleReceiveMessage = (firstName: string, lastName: string) => {
        console.log("FirstName: " + firstName);
        console.log("LastName: " + lastName);

        setfirstName(firstName);
        setlastName(lastName);
    }

    const sendMessage = async () => {
        if (connection)
            await connection.send("GetCustomersIdAsync", "a6c8c6b1-4349-45b0-ab31-244740aaf0f2");
    };

    return (
        <>
            
            <Button onClick={sendMessage} type="primary">
                Subscribe
            </Button>

            <h1>FirstName: {firstName}</h1>
            <h1>LastName: {lastName}</h1>
        </>
    );
};

export default StockItem;