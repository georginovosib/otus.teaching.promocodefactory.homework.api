﻿using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Hubs
{
    public class GreetHub : Hub
    {
        private readonly IRepository<Customer> _customerRepository;

        public GreetHub(IRepository<Customer> customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return await Task.FromResult(response);
        }

        public async Task<string> GetCustomersIdAsync(string id)
        {
            var customers = await _customerRepository.GetByIdAsync(Guid.Parse(id));

            var firstName = customers.FirstName;

            var lastName = customers.LastName;

            await Clients.All.SendAsync("Receive", firstName, lastName);

            return await Task.FromResult("FirstName: " + " " + firstName);
        }

        public async Task<string> DeleteCustomerAsync(string id)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(id));

            await _customerRepository.DeleteAsync(customer);

            return await Task.FromResult("Delete: " + " " + id);
        }

        public async Task<string> CreateCustomerAsync(CustomerShortResponse c)
        {

            Customer customer = new Customer() { Id = c.Id, FirstName = c.FirstName, LastName = c.LastName, Email = c.Email };

            await _customerRepository.AddAsync(customer);

            return await Task.FromResult("Клиент создан");
        }

        public async Task<string> CreateOrEditCustomerRequest(CustomerShortResponse c)
        {

            var customer = await _customerRepository.GetByIdAsync(c.Id);

            Customer cust = new Customer() { Id = c.Id, FirstName = c.FirstName, LastName = c.LastName, Email = c.Email };

            await _customerRepository.UpdateAsync(cust);

            return await Task.FromResult("Клиент обновлен");
        }

        public async Task SendFromReact(string message)
        {
            await Clients.All.SendAsync("Receive", message);
        }
    }
}
