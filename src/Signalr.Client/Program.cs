﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Signalr.Client
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            Guid id = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");

            Console.WriteLine($"client: {id}");

            var connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:5001/hubs/greet")
                .WithAutomaticReconnect()
                .Build();

            await connection.StartAsync();

            // Получить всех клиентов
            List<CustomerShortResponse> lists = await connection.InvokeAsync<List<CustomerShortResponse>>("GetCustomersAsync");

            foreach (var item in lists)
            {
                Console.WriteLine($"server answer: {item.FirstName}");
            }

            // Получить клиента по id
            var s = await connection.InvokeAsync<string>("GetCustomersIdAsync", id);

            Console.WriteLine(s);

            // Создать клиента
            var c = new CustomerShortResponse() { Id = Guid.NewGuid(), FirstName = "Семен", LastName = "Семенович", Email = "s@s.s" };

            var create = await connection.InvokeAsync<string>("CreateCustomerAsync", c);

            Console.WriteLine(create);

            // Обновить клиента
            var u = new CustomerShortResponse() { Id = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), FirstName = "Виктор", LastName = "Алексеевич", Email = "v@v.v" };

            var update = await connection.InvokeAsync<string>("CreateOrEditCustomerRequest", u);

            Console.WriteLine(update);

            // Удалить клиента по id
            var d = await connection.InvokeAsync<string>("DeleteCustomerAsync", id);

            Console.WriteLine(d);

            Console.ReadLine();

            await connection.StopAsync();
        }
    }

    public class CustomerShortResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}
